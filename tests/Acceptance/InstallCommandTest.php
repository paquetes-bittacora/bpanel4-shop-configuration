<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ShopConfiguration\Tests\Acceptance;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class InstallCommandTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testElComandoDeInstalacionSeEjecuta(): void
    {
        $result = $this->artisan('bpanel4-shop-configuration:install');
        $result->assertSuccessful();
    }
}
