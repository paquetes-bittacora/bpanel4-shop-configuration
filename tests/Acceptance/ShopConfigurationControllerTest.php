<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ShopConfiguration\Tests\Acceptance;

use Bittacora\Bpanel4\ShopConfiguration\Models\ShopConfiguration;
use Bittacora\Bpanel4Users\Database\Factories\UserFactory;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

final class ShopConfigurationControllerTest extends TestCase
{
    use RefreshDatabase;

    private UrlGenerator $urlGenerator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->urlGenerator = $this->app->make(UrlGenerator::class);
        $this->withoutExceptionHandling();

        Role::create(['name' => 'admin']);
        $this->actingAs((new UserFactory())->admin()
            ->withPermissions(
                'bpanel4-shop-configuration.bpanel.edit',
                'bpanel4-shop-configuration.bpanel.update'
            )->createOne());
        ShopConfiguration::firstOrCreate();
    }

    public function testCargaLaPaginaDeEdicionDeConfiguracionDeLaTienda(): void
    {
        $response = $this->get($this->urlGenerator->route('bpanel4-shop-configuration.bpanel.edit'));
        $response->assertOk();
        $response->assertSee('Configuración de la tienda');
    }

    public function testPuedeGuardarLaConfiguracionDeLaTienda(): void
    {
        $response = $this->followingRedirects()->put($this->urlGenerator->route('bpanel4-shop-configuration.bpanel.update'), [
            'name' => 'nombre editado',
            'cif' => 'cif editado',
            'address' => 'dirección editada',
            'invoicePrefix' => 'prefijo-',
            'invoiceSuffix' => '-sufijo',
        ]);

        $response->assertOk();
        $response->assertSee('Configuración actualizada');
    }
}
