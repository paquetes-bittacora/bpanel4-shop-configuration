<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ShopConfiguration\Tests\Helpers;

use Bittacora\Bpanel4\ShopConfiguration\Models\ShopConfiguration;

final class ShopConfigurationTestHelper
{
    public static function createEmptyShopConfiguration(): void
    {
        $shopConfig = new ShopConfiguration();
        $shopConfig->setName('Prueba');
        $shopConfig->setCif('00000000X');
        $shopConfig->setAddress('Dirección de prueba');
        $shopConfig->save();
    }
}