<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ShopConfiguration\Tests\Functional;

use Bittacora\Bpanel4\ShopConfiguration\Actions\SaveShopConfiguration;
use Bittacora\Bpanel4\ShopConfiguration\Dtos\ShopConfigurationDto;
use Bittacora\Bpanel4\ShopConfiguration\Models\ShopConfiguration;
use Exception;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Tests\TestCase;

final class SaveShopConfigurationTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        ShopConfiguration::create();
    }

    /**
     * @throws Exception
     */
    public function testGuardaLaConfiguracionDeLaTienda(): void
    {
        $randomString = bin2hex(random_bytes(2));
        /** @var SaveShopConfiguration $saveShopConfig */
        $saveShopConfig = $this->app->make(SaveShopConfiguration::class);
        $saveShopConfig->handle(new ShopConfigurationDto(
            address: 'Dirección de la tienda' . $randomString,
            cif: 'CIF de la tienda' . $randomString,
            name: 'Nombre de la tienda' . $randomString,
            invoicePrefix: 'prefijo-' . $randomString,
            invoiceSuffix: '-suffix' . $randomString,
        ));

        $shopConfig = ShopConfiguration::whereId(1)->firstOrFail();
        self::assertEquals($shopConfig->getAddress(), 'Dirección de la tienda' . $randomString);
        self::assertEquals($shopConfig->getCif(), 'CIF de la tienda' . $randomString);
        self::assertEquals($shopConfig->getName(), 'Nombre de la tienda' . $randomString);
        self::assertEquals($shopConfig->getInvoicePrefix(), 'prefijo-' . $randomString);
        self::assertEquals($shopConfig->getInvoiceSuffix(), '-suffix' . $randomString);
    }

    public function testGuardaElLogotipoDeLasFacturas(): void
    {
        Storage::fake();
        $file = UploadedFile::fake()->image('logo-facturas.jpg');

        /** @var SaveShopConfiguration $saveShopConfig */
        $saveShopConfig = $this->app->make(SaveShopConfiguration::class);

        $saveShopConfig->handle(new ShopConfigurationDto(
            address: 'Dirección de la tienda',
            cif: 'CIF de la tienda',
            name: 'Nombre de la tienda',
            invoicePrefix: 'prefijo-',
            invoiceSuffix: '-suffix',
            invoiceLogo: $file,
        ));

        $shopConfig = ShopConfiguration::whereId(1)->firstOrFail();
        self::assertInstanceOf(Media::class, $shopConfig->getInvoiceLogo());
    }
}
