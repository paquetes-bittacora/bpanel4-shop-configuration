<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ShopConfiguration\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * @method static self whereId($id)
 * @method self firstOrFail()
 * @method static self create()
 * @property ?string $address
 * @property ?string $name
 * @property ?string $cif
 * @property ?string $invoice_prefix
 * @property ?string $invoice_suffix
 */
final class ShopConfiguration extends Model implements HasMedia
{
    use InteractsWithMedia;

    /** @var string  */
    public $table = 'shop_configuration';

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getCif(): ?string
    {
        return $this->cif;
    }

    public function getInvoicePrefix(): string
    {
        return $this->invoice_prefix ?? '';
    }

    public function getInvoiceSuffix(): string
    {
        return $this->invoice_suffix ?? '';
    }

    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function setCif(?string $cif): void
    {
        $this->cif = $cif;
    }

    public function setInvoicePrefix(?string $invoicePrefix): void
    {
        $this->invoice_prefix = $invoicePrefix;
    }

    public function setInvoiceSuffix(?string $invoiceSuffix): void
    {
        $this->invoice_suffix = $invoiceSuffix;
    }

    public function getInvoiceLogo(): null|Media|Model
    {
        return $this->media()->first();
    }
}
