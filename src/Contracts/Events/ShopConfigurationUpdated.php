<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ShopConfiguration\Contracts\Events;

use Bittacora\Bpanel4\ShopConfiguration\Models\ShopConfiguration;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Http\Request;

final class ShopConfigurationUpdated
{
    use Dispatchable;

    public function __construct(public readonly Request $request)
    {
    }
}