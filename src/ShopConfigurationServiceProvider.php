<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ShopConfiguration;

use Bittacora\Bpanel4\ShopConfiguration\Commands\InstallCommand;
use Illuminate\Support\ServiceProvider;

final class ShopConfigurationServiceProvider extends ServiceProvider
{
    private const PACKAGE_PREFIX = 'bpanel4-shop-configuration';

    public function boot(): void
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->commands(InstallCommand::class);
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', self::PACKAGE_PREFIX);
    }
}
