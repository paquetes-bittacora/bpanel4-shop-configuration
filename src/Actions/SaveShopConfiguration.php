<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ShopConfiguration\Actions;

use Bittacora\Bpanel4\ShopConfiguration\Dtos\ShopConfigurationDto;
use Bittacora\Bpanel4\ShopConfiguration\Models\ShopConfiguration;
use Exception;
use Illuminate\Database\Connection;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Throwable;

final class SaveShopConfiguration
{
    public function __construct(private readonly Connection $connection)
    {
    }

    /**
     * @throws Throwable
     */
    public function handle(ShopConfigurationDto $dto): void
    {
        $this->connection->beginTransaction();
        try {
            $shopConfiguration = ShopConfiguration::whereId(1)->firstOrFail();

            $shopConfiguration->setName($dto->name);
            $shopConfiguration->setAddress($dto->address);
            $shopConfiguration->setCif($dto->cif);
            $shopConfiguration->setInvoicePrefix($dto->invoicePrefix);
            $shopConfiguration->setInvoiceSuffix($dto->invoiceSuffix);
            $this->uploadAvatar($shopConfiguration, $dto);
            $shopConfiguration->save();
            $this->connection->commit();
        } catch (Exception $exception) {
            report($exception);
            $this->connection->rollBack();
            throw $exception;
        }
    }

    /**
     * @throws FileIsTooBig
     * @throws FileDoesNotExist
     */
    public function uploadAvatar(ShopConfiguration $shopConfiguration, ShopConfigurationDto $dto): void
    {
        if (null === $dto->invoiceLogo) {
            return;
        }

        foreach ($shopConfiguration->media()->get()->all() as $media) {
            $media->delete();
        }

        $shopConfiguration->addMedia($dto->invoiceLogo)->toMediaCollection();
    }
}
