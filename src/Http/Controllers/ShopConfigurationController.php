<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ShopConfiguration\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\ShopConfiguration\Actions\SaveShopConfiguration;
use Bittacora\Bpanel4\ShopConfiguration\Contracts\Events\ShopConfigurationUpdated;
use Bittacora\Bpanel4\ShopConfiguration\Dtos\ShopConfigurationDto;
use Bittacora\Bpanel4\ShopConfiguration\Models\ShopConfiguration;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Events\Dispatcher;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use InvalidArgumentException;

final class ShopConfigurationController extends Controller
{
    public function __construct(
        private readonly Factory $view,
        private readonly Redirector $redirector,
        private readonly Dispatcher $dispatcher,
    ) {
    }

    public function edit(): View
    {
        $this->authorize('bpanel4-shop-configuration.bpanel.edit');

        return $this->view->make('bpanel4-shop-configuration::bpanel.edit', [
            'shopConfiguration' => ShopConfiguration::firstOrFail(),
        ]);
    }

    public function update(Request $request, SaveShopConfiguration $saveShopConfiguration): RedirectResponse
    {
        $this->authorize('bpanel4-shop-configuration.bpanel.update');
        try {
            $saveShopConfiguration->handle(new ShopConfigurationDto(...$request->only([
                'name',
                'address',
                'cif',
                'invoicePrefix',
                'invoiceSuffix',
                'invoiceLogo'
            ])));
            $this->dispatcher->dispatch(new ShopConfigurationUpdated($request));
            return $this->redirector->route('bpanel4-shop-configuration.bpanel.edit')
                ->with(['alert-success' => 'Configuración actualizada']);
        } catch (Exception $e) {
            report($e);
            return $this->redirector->route('bpanel4-shop-configuration.bpanel.edit')
                ->with(['alert-danger' => 'No se pudo guardar la configuración. ' . $e->getMessage()]);
        }
    }
}
