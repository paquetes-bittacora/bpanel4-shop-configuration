<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ShopConfiguration\Commands;

use Bittacora\AdminMenu\AdminMenu;
use Bittacora\Bpanel4\ShopConfiguration\Models\ShopConfiguration;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

final class InstallCommand extends Command
{
    /** @var string */
    protected $signature = 'bpanel4-shop-configuration:install';

    /** @var string */
    protected $description = 'Instala el paquete para introducir la configuración básica de la tienda.';

    public function handle(AdminMenu $adminMenu): void
    {
        $this->createMenuEntries($adminMenu);
        $this->giveAdminPermissions();
        $this->createEmptyConfiguration();
    }

    private function createEmptyConfiguration(): void
    {
        $config = ShopConfiguration::firstOrCreate();
        $config->name = config('app.name');
        $config->address = '';
        $config->cif = '';
        $config->invoice_prefix = '';
        $config->invoice_suffix = '';
        $config->save();
    }

    private function createMenuEntries(AdminMenu $adminMenu): void
    {
        $this->comment('Añadiendo módulo de configuración de la tienda al menú de administración...');

        $adminMenu->createGroup('ecommerce', 'Tienda', 'far fa-shopping-cart');
        $adminMenu->createModule(
            'ecommerce',
            'bpanel4-shop-configuration.bpanel',
            'Configuración de la tienda',
            'far fa-cog'
        );
        $adminMenu->createAction(
            'bpanel4-shop-configuration.bpanel',
            'Editar',
            'edit',
            'far fa-pencil'
        );
    }

    private function giveAdminPermissions(): void
    {
        $this->comment('Añadiendo permisos...');
        $adminRole = Role::findOrCreate('admin');
        $adminRole->givePermissionTo(Permission::firstOrCreate(['name' => 'bpanel4-shop-configuration.bpanel.edit']));
        $adminRole->givePermissionTo(Permission::firstOrCreate(['name' => 'bpanel4-shop-configuration.bpanel.update']));
    }
}
