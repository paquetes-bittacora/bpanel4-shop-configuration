<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ShopConfiguration\Dtos;

use Illuminate\Http\UploadedFile;

final class ShopConfigurationDto
{
    public function __construct(
        public readonly string $address,
        public readonly string $cif,
        public readonly string $name,
        public readonly ?string $invoicePrefix = '',
        public readonly ?string $invoiceSuffix = '',
        public readonly ?UploadedFile $invoiceLogo = null,
    ) {
    }
}
