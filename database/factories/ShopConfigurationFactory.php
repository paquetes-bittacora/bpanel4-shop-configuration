<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ShopConfigurations\Database\Factories;

use Bittacora\Bpanel4\ShopConfiguration\Models\ShopConfiguration;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<ShopConfiguration>
 * @method ShopConfiguration createOne($attributes = [])
 * @method self count(?int $count)
 * @method Collection<int, ShopConfiguration> create()
 */
class ShopConfigurationFactory extends Factory
{
    /**
     * @var class-string<ShopConfiguration>
     */
    protected $model = ShopConfiguration::class;

    /**
     * @throws InvalidPriceException
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'address' => $this->faker->text,
            'cif' => '00000000A',
            'invoice_prefix' => '',
            'invoice_suffix' => '',
        ];
    }
}
