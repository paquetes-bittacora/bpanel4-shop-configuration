<?php

declare(strict_types=1);

use Bittacora\Bpanel4\ShopConfiguration\Http\Controllers\ShopConfigurationController;
use Illuminate\Support\Facades\Route;

Route::prefix('bpanel/tienda')->name('bpanel4-shop-configuration.bpanel.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(static function () {
        Route::get('/editar-configuracion', [ShopConfigurationController::class, 'edit'])->name('edit');
        Route::put('/guardar-configuracion', [ShopConfigurationController::class, 'update'])->name('update');
    });
