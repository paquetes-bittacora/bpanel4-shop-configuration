@extends('bpanel4::layouts.bpanel-app')

@section('title', __('bpanel4-shop-configuration::configuration.shop-configuration'))

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('bpanel4-shop-configuration::configuration.shop-configuration') }}</span>
            </h4>
        </div>
        <form class="mt-lg-3" autocomplete="off" method="post" action="{{ route('bpanel4-shop-configuration.bpanel.update') }}"
            enctype="multipart/form-data">
            @livewire('form::input-text', ['name' => 'name', 'labelText' =>
            __('bpanel4-shop-configuration::configuration.name'), 'required'=>true, 'value' => old('name') ??
            $shopConfiguration?->getName() ])
            @livewire('form::input-text', ['name' => 'cif', 'labelText' =>
            __('bpanel4-shop-configuration::configuration.cif'), 'required'=>true, 'value' => old('cif') ??
            $shopConfiguration?->getCif() ])
            @livewire('form::textarea', ['name' => 'address', 'labelText' =>
            __('bpanel4-shop-configuration::configuration.address'), 'value' => old('address') ??
            $shopConfiguration?->getAddress() ])
            {{ Bp4Hook::execute('shop-config-before-invoice-fields') }}
            @livewire('form::input-text', ['name' => 'invoicePrefix', 'labelText' =>
            __('bpanel4-shop-configuration::configuration.invoicePrefix'), 'required'=>false, 'value' =>
            old('invoicePrefix') ?? $shopConfiguration?->getinvoicePrefix() ])
            @livewire('form::input-text', ['name' => 'invoiceSuffix', 'labelText' =>
            __('bpanel4-shop-configuration::configuration.invoiceSuffix'), 'required'=>false, 'value' =>
            old('invoiceSuffix') ?? $shopConfiguration?->getinvoiceSuffix() ])

            @if (null !== $shopConfiguration->getInvoiceLogo())
                <div class="form-group form-row">
                    <div class="col-sm-3 col-form-label text-sm-right">
                        <label for="id-form-field-1" class="mb-0  ">
                            Logotipo actual para las facturas:
                        </label>
                    </div>

                    <div class="col-sm-7">
                        <img src="{{ $shopConfiguration->getInvoiceLogo()->getFullUrl() }}" style="max-width: 500px;">
                    </div>
                </div>
            @endif
            @livewire('form::input-file', ['name' => 'invoiceLogo', 'labelText' => 'Logotipo para las facturas',
            'maxFileCount' => 1, 'accept' => '.jpg,.jpeg,.png', 'allowedFileExtensions' => 'jpg,jpeg,png' ])
            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form::save-button',['theme'=>'update'])
                @livewire('form::save-button',['theme'=>'reset'])
            </div>
            @method('put')
            @csrf
        </form>
    </div>
@endsection
