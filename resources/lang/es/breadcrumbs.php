<?php

declare(strict_types=1);

return [
    'bpanel4-shop-configuration' => 'Configuración de la tienda',
    'bpanel' => 'Editar',
];
