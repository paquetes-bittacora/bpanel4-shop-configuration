<?php

declare(strict_types=1);

return [
    'edit-shop-configuration' => 'Editar configuración de la tienda',
    'address' => 'Dirección',
    'shop-configuration' => 'Configuración de la tienda',
    'name' => 'Nombre o razón social de la tienda',
    'cif' => 'CIF',
    'invoicePrefix' => 'Prefijo del número de factura',
    'invoiceSuffix' => 'Sufijo del número de factura',
];
